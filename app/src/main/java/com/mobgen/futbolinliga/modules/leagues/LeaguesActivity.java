package com.mobgen.futbolinliga.modules.leagues;

import android.animation.ObjectAnimator;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.mobgen.futbolinliga.R;
import com.mobgen.futbolinliga.modules.leagues.adapters.LeaguePagerAdapter;
import com.mobgen.futbolinliga.shared.clasifications.models.League;
import com.mobgen.futbolinliga.shared.clasifications.usecases.GetLeaguesUseCase;
import com.mobgen.futbolinliga.shared.clasifications.usecases.IGetLeaguesUseCase;

import java.util.ArrayList;

public class LeaguesActivity extends AppCompatActivity implements IGetLeaguesUseCase {

    private LeaguePagerAdapter adapterViewPager;

    private MenuItem refreshButton;
    private Boolean isRefreshing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        refresh();
    }

    @Override
    public void success(ArrayList<League> leagues) {

        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        if (adapterViewPager == null)
        {
            adapterViewPager = new LeaguePagerAdapter(getSupportFragmentManager(), leagues);
            vpPager.setAdapter(adapterViewPager);
        }
        else
        {
            adapterViewPager.setLeagues(leagues);
        }
        if (refreshButton != null) refreshButton.setEnabled(true);
        isRefreshing = false;
    }

    private void refresh() {

        if (isRefreshing) return;
        isRefreshing = true;

        if (refreshButton != null) refreshButton.setEnabled(false);
        GetLeaguesUseCase useCase = new GetLeaguesUseCase();
        useCase.delegate = this;
        useCase.getLeagues();
    }


}
