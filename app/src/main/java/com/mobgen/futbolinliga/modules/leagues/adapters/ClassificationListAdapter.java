package com.mobgen.futbolinliga.modules.leagues.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mobgen.futbolinliga.R;
import com.mobgen.futbolinliga.shared.clasifications.models.Team;

import java.util.ArrayList;

/**
 * Created by mike on 25/05/16.
 */

public class ClassificationListAdapter extends ArrayAdapter<Team> {

    public static class Conditions
    {
        final int nWiners;
        final int nLoosers;
        final int nPlayoff;

        public Conditions(int nWiners, int nLoosers, int nPlayoff) {
            this.nWiners = nWiners;
            this.nLoosers = nLoosers;
            this.nPlayoff = nPlayoff;
        }
    }

    private Context context;
    private int layoutResourceId;
    private ArrayList<Team> teams = null;
    private Conditions conditions;


    public ClassificationListAdapter(Context context, int layoutResourceId, ArrayList<Team> data, Conditions conditions) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.teams = data;
        this.conditions = conditions;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        TeamHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new TeamHolder();
            holder.frontPlayerTextView = (TextView)row.findViewById(R.id.front_player_tv);
            holder.backPlayerTextView = (TextView)row.findViewById(R.id.back_player_tv);
            holder.painsTextView = (TextView)row.findViewById(R.id.points_tv);
            holder.playedGamesTextView = (TextView)row.findViewById(R.id.played_games_tv);
            holder.gamesWonTextView = (TextView)row.findViewById(R.id.game_won_tv);
            holder.gamesTiedTextView = (TextView)row.findViewById(R.id.game_tied_tv);
            holder.gamesLostTextView = (TextView)row.findViewById(R.id.game_lost_tv);
            holder.goalAverageTextView = (TextView)row.findViewById(R.id.goal_average_tv);
            holder.statusIndicator = (FrameLayout)row.findViewById(R.id.status_indicator);

            row.setTag(holder);
        }
        else
        {
            holder = (TeamHolder)row.getTag();
        }

        Team team = teams.get(position);
        holder.frontPlayerTextView.setText(team.getFrontPlayer());
        holder.backPlayerTextView.setText(team.getBackPlayer());
        holder.painsTextView.setText(team.getPoints());

        int gamesWon = Integer.parseInt(team.getWonGames());
        int gamesTied = Integer.parseInt(team.getDrawGames());
        int gamesLost = Integer.parseInt(team.getLostGames());

        String playedGames = String.valueOf(gamesWon + gamesTied + gamesLost);
        String wonGames = String.valueOf(gamesWon);
        String tiedGames = String.valueOf(gamesTied);
        String lostGames = String.valueOf(gamesLost);

        holder.playedGamesTextView.setText(playedGames);
        holder.gamesWonTextView.setText(wonGames);
        holder.gamesTiedTextView.setText(tiedGames);
        holder.gamesLostTextView.setText(lostGames);

        int goalsInFavor = Integer.parseInt(team.getGoalsFor());
        int goalsAgainst = Integer.parseInt(team.getGoalsAgainst());
        int goalAvg = goalsInFavor - goalsAgainst;

        String goalAverage = String.valueOf(goalAvg);

        holder.goalAverageTextView.setText(goalAverage);

        if (position < conditions.nWiners)
        {
            holder.statusIndicator.setBackgroundColor(context.getResources().getColor(R.color.emeraldGreen));
        }
        else if (position >= teams.size() - conditions.nLoosers)
        {
            holder.statusIndicator.setBackgroundColor(context.getResources().getColor(R.color.pomegranate));
        }
        else if (position < conditions.nPlayoff + conditions.nWiners)
        {
            holder.statusIndicator.setBackgroundColor(context.getResources().getColor(R.color.peter_river));
        }
        else
        {
            holder.statusIndicator.setBackgroundColor(context.getResources().getColor(R.color.silver));
        }

        return row;
    }

    static class TeamHolder
    {
        TextView frontPlayerTextView;
        TextView backPlayerTextView;
        TextView painsTextView;
        TextView playedGamesTextView;
        TextView gamesWonTextView;
        TextView gamesTiedTextView;
        TextView gamesLostTextView;
        TextView goalAverageTextView;
        FrameLayout statusIndicator;
    }

}
