package com.mobgen.futbolinliga.modules.leagues.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mobgen.futbolinliga.shared.clasifications.models.League;
import com.mobgen.futbolinliga.modules.leagues.fragments.LeagueFragment;

import java.util.ArrayList;

/**
 * Created by mike on 25/05/16.
 */

public class LeaguePagerAdapter extends FragmentPagerAdapter {

    private ArrayList<League> leagues = new ArrayList<>();

    public LeaguePagerAdapter(FragmentManager fm, ArrayList<League> data) {
        super(fm);

        this.leagues = data;
    }

    public void setLeagues(ArrayList<League> leagues) {
        this.leagues = leagues;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {

        return LeagueFragment.newInstance(position, leagues.get(position));
    }

    @Override
    public int getCount() {
        return leagues.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return leagues.get(position).getName();
    }
}
