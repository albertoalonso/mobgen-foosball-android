package com.mobgen.futbolinliga.modules.leagues.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mobgen.futbolinliga.R;
import com.mobgen.futbolinliga.modules.leagues.adapters.ClassificationListAdapter;
import com.mobgen.futbolinliga.shared.clasifications.models.League;

/**
 * Created by mike on 25/05/16.
 */

public class LeagueFragment extends Fragment {

    private static final String PARCELABLE_PAGE = "page";
    private static final String PARCELABLE_LEAGUE = "league";

    private int page;
    private League league;

    public static LeagueFragment newInstance(int page, League league) {
        LeagueFragment fragment = new LeagueFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(PARCELABLE_PAGE, page);
        bundle.putParcelable(PARCELABLE_LEAGUE, league);
        fragment.setArguments(bundle);
        return fragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        this.page = bundle.getInt(PARCELABLE_PAGE);
        this.league = bundle.getParcelable(PARCELABLE_LEAGUE);

    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_league, container, false);

        ListView listView = (ListView) view.findViewById(R.id.listView);
        ClassificationListAdapter adapter = new ClassificationListAdapter(getContext(), R.layout.classifications_team_list_item, league.getTeams(), getCondition(page));
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        return view;
    }

    private ClassificationListAdapter.Conditions getCondition(int leagueID)
    {
        switch (leagueID) {
            case 0 :
                return new ClassificationListAdapter.Conditions(1, 3, 0);
            case 1 :
                return new ClassificationListAdapter.Conditions(2, 0, 2);
        }

        return new ClassificationListAdapter.Conditions(2, 2, 0);
    }

}
