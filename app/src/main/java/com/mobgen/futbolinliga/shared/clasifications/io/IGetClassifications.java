package com.mobgen.futbolinliga.shared.clasifications.io;

import com.mobgen.futbolinliga.shared.clasifications.models.League;

import java.util.ArrayList;

/**
 * Created by mike on 25/05/16.
 */

public interface IGetClassifications {

    void fetchedLeagues(ArrayList<League> teams);

}
