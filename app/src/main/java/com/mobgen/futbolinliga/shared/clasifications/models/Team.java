package com.mobgen.futbolinliga.shared.clasifications.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

/**
 * Created by mike on 25/05/16.
 */

public class Team implements Parcelable {

    private static final String DRAW_GAMES = "draw_games";
    private static final String GOALS_AGAINST = "goals_against";
    private static final String GOALS_DIFFERENCE = "goals_diference";
    private static final String GOALS_FOR = "goals_for";
    private static final String LOST_GAMES = "lost_games";
    private static final String PLAYERS = "players";
    private static final String POINTS = "points";
    private static final String POSITION = "position";
    private static final String WON_GAMES = "win_games";

    private String drawGames;
    private String goalsAgainst;
    private String goalsDifference;
    private String goalsFor;
    private String lostGames;
    private String players;
    private String frontPlayer;
    private String backPlayer;
    private String points;
    private String position;
    private String wonGames;

    //Static Methods

    public static Team Create(JSONObject object) {

        if (object == null) return null;

        Team team = new Team();

        try {

            team.drawGames = object.getString(DRAW_GAMES);
            team.goalsAgainst = object.getString(GOALS_AGAINST);
            team.goalsDifference = object.getString(GOALS_DIFFERENCE);
            team.goalsFor = object.getString(GOALS_FOR);
            team.lostGames = object.getString(LOST_GAMES);
            team.players = object.getString(PLAYERS);

            String[] players = team.getPlayers().split(" - ");
            team.frontPlayer = players[0];
            team.backPlayer = players[1];

            team.points = object.getString(POINTS);
            team.position = object.getString(POSITION);
            team.wonGames = object.getString(WON_GAMES);

        }
        catch (Exception e) {

            e.printStackTrace();
            return null;
        }

        return team;
    }

    //Constructors

    public Team() {
    }

    //Getters

    public String getDrawGames() {
        return drawGames;
    }

    public String getGoalsAgainst() {
        return goalsAgainst;
    }

    public String getGoalsDifference() {
        return goalsDifference;
    }

    public String getGoalsFor() {
        return goalsFor;
    }

    public String getLostGames() {
        return lostGames;
    }

    public String getPlayers() {
        return players;
    }

    public String getFrontPlayer() {
        return frontPlayer;
    }

    public String getBackPlayer() {
        return backPlayer;
    }

    public String getPoints() {
        return points;
    }

    public String getPosition() {
        return position;
    }

    public String getWonGames() {
        return wonGames;
    }


    //Parcelling Part

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.drawGames);
        dest.writeString(this.goalsAgainst);
        dest.writeString(this.goalsDifference);
        dest.writeString(this.goalsFor);
        dest.writeString(this.lostGames);
        dest.writeString(this.players);
        dest.writeString(this.frontPlayer);
        dest.writeString(this.backPlayer);
        dest.writeString(this.points);
        dest.writeString(this.position);
        dest.writeString(this.wonGames);
    }

    protected Team(Parcel in) {
        this.drawGames = in.readString();
        this.goalsAgainst = in.readString();
        this.goalsDifference = in.readString();
        this.goalsFor = in.readString();
        this.lostGames = in.readString();
        this.players = in.readString();
        this.frontPlayer = in.readString();
        this.backPlayer = in.readString();
        this.points = in.readString();
        this.position = in.readString();
        this.wonGames = in.readString();
    }

    public static final Parcelable.Creator<Team> CREATOR = new Parcelable.Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel source) {
            return new Team(source);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };
}
