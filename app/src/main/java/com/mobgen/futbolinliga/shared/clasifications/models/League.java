package com.mobgen.futbolinliga.shared.clasifications.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mike on 25/05/16.
 */

public class League implements Parcelable {

    private static final String LEADERBOARD = "leaderboard";
    private static final String NAME = "name";

    private String name;
    private ArrayList<Team> teams = new ArrayList<>();

    //Static Methods

    public static  League Create(JSONObject object)
    {
        League league = new League();

        try {

            league.name = object.getString(NAME);

            JSONArray array = object.getJSONArray(LEADERBOARD);
            for (int i = 0; i < array.length(); i++) {

                Team team = Team.Create(array.getJSONObject(i));
                if (team != null) {
                    league.teams.add(team);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

        return league;
    }

    //Constructors

    public League() {
    }

    //Getters

    public String getName() {
        return name;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }


    //Parcelling Methods

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeList(this.teams);
    }


    protected League(Parcel in) {
        this.name = in.readString();
        this.teams = new ArrayList<>();
        in.readList(this.teams, Team.class.getClassLoader());
    }

    public static final Creator<League> CREATOR = new Creator<League>() {
        @Override
        public League createFromParcel(Parcel source) {
            return new League(source);
        }

        @Override
        public League[] newArray(int size) {
            return new League[size];
        }
    };
}
