package com.mobgen.futbolinliga.shared.clasifications.usecases;

import com.mobgen.futbolinliga.shared.clasifications.models.League;

import java.util.ArrayList;

/**
 * Created by mike on 25/05/16.
 */

public interface IGetLeaguesUseCase {

    void success(ArrayList<League> teams);

}
