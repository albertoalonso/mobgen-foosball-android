package com.mobgen.futbolinliga.shared.clasifications.io;

import com.mobgen.futbolinliga.shared.clasifications.models.League;
import com.mobgen.futbolinliga.io.IRequestTask;
import com.mobgen.futbolinliga.io.RequestTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by mike on 25/05/16.
 */

public class GetClasifications implements IRequestTask{

    private static final String URL_STRING = "https://luminous-torch-2242.firebaseio.com/kimono/api/94wdsmie/latest.json?auth=WU6EmqmAyBxGdZAYCDjUnRCGq2yzf0weU2BWy0Zi";

    private static final String LEAGUES = "leagues";

    public IGetClassifications delegate;

    public void fetchLeagues ()
    {
        try
        {
            RequestTask task = new RequestTask(this, "GET");
            task.execute(new URL(URL_STRING));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void fetched(JSONObject object) {

        ArrayList<League> leagues = new ArrayList<>();
        try {

            JSONArray array = object.getJSONArray(LEAGUES);

            for (int i = 0; i < array.length(); i++)
            {
                League league = League.Create(array.getJSONObject(i));
                if (league != null) {
                    leagues.add(league);
                }
            }

        }
        catch (JSONException e) {

            e.printStackTrace();
        }

        if (delegate != null)
        {
            delegate.fetchedLeagues(leagues);
        }

    }
}
