package com.mobgen.futbolinliga.shared.clasifications.usecases;

import com.mobgen.futbolinliga.shared.clasifications.io.GetClasifications;
import com.mobgen.futbolinliga.shared.clasifications.io.IGetClassifications;
import com.mobgen.futbolinliga.shared.clasifications.models.League;

import java.util.ArrayList;

/**
 * Created by mike on 25/05/16.
 */

public class GetLeaguesUseCase implements IGetClassifications
{
    public IGetLeaguesUseCase delegate;

    public void getLeagues()
    {
        GetClasifications getClasifications = new GetClasifications();
        getClasifications.delegate = this;
        getClasifications.fetchLeagues();
    }

    @Override
    public void fetchedLeagues(ArrayList<League> leagues) {
        if (delegate != null)
        {
            delegate.success(leagues);
        }
    }

}
