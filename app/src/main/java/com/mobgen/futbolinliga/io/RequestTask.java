package com.mobgen.futbolinliga.io;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by mike on 25/05/16.
 */

public class RequestTask extends AsyncTask<URL, Void, JSONObject> {

    public static String GET = "GET";
    public static String POST = "POST";

    private IRequestTask delegate;
    private String requestType;

    public RequestTask(IRequestTask delegate, String requestType)
    {
        super();
        this.delegate = delegate;
        this.requestType = requestType;
    }

    @Override
    protected JSONObject doInBackground(URL... params) {

        if (requestType == null) return null;

        JSONObject result = null;

        try
        {
            HttpURLConnection connection = (HttpURLConnection) params[0].openConnection();
            connection.setRequestMethod(requestType);

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            br.close();


            JSONObject object = new JSONObject(sb.toString());
            result = object;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

        return result;
    }

    @Override
    protected void onPostExecute(JSONObject object) {
        super.onPostExecute(object);

        if (delegate != null && object != null)
        {
            delegate.fetched(object);
        }
    }

}
