package com.mobgen.futbolinliga.io;

import org.json.JSONObject;

/**
 * Created by mike on 25/05/16.
 */

public interface IRequestTask {

    void fetched(JSONObject object);

}
